'use strict';

module.exports = {
  url: 'https://tuanducdesign.com',
  pathPrefix: '/',
  title: 'Tuan Duc Design',
  subtitle: 'Tuan Duc Design. Chuyên trang chia sẻ các kiến thức liên quan đến phát triển VPS và tối ưu Website',
  copyright: '© tuanducdesign.com',
  disqusShortname: '',
  useCommento: false,
  facebookComment: {
    active: true,
    appId: 294587991860787,
  },
  postsPerPage: 8,
  //googleAnalyticsId: 'UA-175649416-1',
  //googleTagManagerId: 'GTM-TV2RCFN',
  //useKatex: false,
  menu: [
    {
      label: 'Trang chủ',
      path: '/'
    },
    {
      label: 'Về tôi',
      path: '/pages/about'
    },
    {
      label: 'Liên hệ',
      path: '/pages/contacts'
    },
    {
      label: 'Khách hàng',
      path: '/pages/my-customer'
    },
    {
      label: 'Thể loại',
      path: '/categories'
    },
    {
      label: 'Thẻ',
      path: '/tags'
    },
    {
      label: 'Dịch vụ',
      path: 'https://service.tuanducdesign.com/'
    }
  ],
  author: {
    name: 'Tuan Duc Design',
    photo: '/tuanducdesign.jpg',
    bio: 'Chuyên trang chia sẻ các kiến thức liên quan đến phát triển VPS và tối ưu Website.',
    contacts: {
      email: 'miumiu.official.56@gmail.com',
      telegram: '',
      twitter: '',
      github: 'tuanducdesign',
      rss: '/rss.xml',
      vkontakte: ''
    }
  }
};
