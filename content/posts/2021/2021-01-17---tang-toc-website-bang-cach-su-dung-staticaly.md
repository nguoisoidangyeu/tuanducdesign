---
template: post
title: 'Tăng Tốc Website Bằng Cách Sử Dụng Staticaly'
date: '2021-01-17T02:00:00.000+07:00'
author: Tuan Duc Tran
category: CDN
tags:
  - Staticaly
  - Tăng Tốc
  - Images
  - Styles
  - JavaScripts
thumbnail: https://1.bp.blogspot.com/-Cbarx1a4YlY/YD_kBRcBulI/AAAAAAAAAaE/N4jEaKvSpWQyJ-PvZa1yxc-KHEXOwaXowCLcBGAsYHQ/-rw/tang-toc-website-bang-cach-su-dung-staticaly.jpg
slug: /2021/01/tang-toc-website-bang-cach-su-dung-staticaly.html
draft: false
description: Theo mình tìm hiểu thì Staticaly khá hay khi sử dụng nó bạn sẽ cảm thấy website của mình nhanh hơn.
fbCommentUrl: https://tuanducdesign.com/2021/01/tang-toc-website-bang-cach-su-dung-staticaly.html
---

## Nội dung bài viết

- [Staticaly là gì](#staticaly-là-gì)
- [Cách ứng dụng Staticaly vào Website](#cách-ứng-dụng-staticaly-vào-website)
- [Tận hưởng thành quả mà Staticaly đem lại](#tận-hưởng-thành-quả-mà-staticaly-đem-lại)

Theo mình tìm hiểu thì Staticaly khá hay khi sử dụng nó bạn sẽ cảm thấy website của mình nhanh hơn

![Tăng Tốc Website Bằng Cách Sử Dụng Staticaly](/../../media/2021/01/tang-toc-website-bang-cach-su-dung-staticaly/tang-toc-website-bang-cach-su-dung-staticaly.jpg)

Trang chủ Staticaly : [https://statically.io](https://statically.io)

## Staticaly là gì

Staticaly là một trang hoàn toàn miễn phí nó giúp bạn đẩy các file tĩnh như ảnh,font,css và js của bạn thông qua URL cdn của họ.

Ví dụ như các ảnh thumb ở ngoài trang chủ trên website của mình.

```html
<img src="https://cdn.staticaly.com/gh/tuanducdesign/tuanducdesign/master/content/media/2021/01/tang-toc-website-bang-cach-su-dung-staticaly/tang-toc-website-bang-cach-su-dung-staticaly.jpg"/>
```

Bất cứ URL ảnh nào bạn chỉ cần nhập vào thông tin trên website của họ là bạn sẽ nhận lại được URL sản phẩm của bạn.

## Cách ứng dụng Staticaly vào Website

Đầu tiên bạn cần truy cập vào đường dẫn dưới đây.

[https://tools.hung1001.com/tools/staticaly.html](https://tools.hung1001.com/tools/staticaly.html)

Tiếp theo bạn kéo xuống cuối trang sau đó dán đường dẫn hình ảnh của bạn vào.

![Miễn Phí CDN Cho Hình Ảnh](/../../media/2021/01/tang-toc-website-bang-cach-su-dung-staticaly/mien-phi-cdn-cho-hinh-anh.jpg)

Sau khi dán thành công bạn sẽ được URL tương tự như này :

```text
https://cdn.staticaly.com/img/service.tuanducdesign.com/images/CA0E67E9-AAD0-4D36-82D8-674C7504DFD1.jpg
```

Về phía đầu trang thì bạn cần phải có một tài khoản Github để có thể sử dụng các chức năng như CDN cho css,js và font.

## Tận hưởng thành quả mà Staticaly đem lại

Giờ bạn có thể tận hưởng thành quả mà Staticaly đem lại cho bạn rồi đó.

Chúc các bạn thành công.