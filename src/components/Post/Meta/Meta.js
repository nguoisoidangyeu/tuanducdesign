// @flow strict
import React from 'react';
import moment from 'moment';
import styles from './Meta.module.scss';

type Props = {
  date: string
};

const Meta = ({ date }: Props) => (
  <div className={styles['meta']}>
    <p className={styles['meta__date']}>Xuất bản {new Date(date).toLocaleDateString('vi-VN', { year: 'numeric', month: 'numeric', day: 'numeric' })}</p>
  </div>
);

export default Meta;
