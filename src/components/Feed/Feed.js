// @flow strict
import React from 'react';
import moment from 'moment';
import { Link } from 'gatsby';
import type { Edges } from '../../types';
import { gtagTrack } from '../../utils';

import styles from './Feed.module.scss';

type Props = {
  edges: Edges
};

const Feed = ({ edges }: Props) => (
  <div className={styles['feed']}>
    {edges.map((edge) => (
      <article className={styles['feed__item']} key={edge.node.fields.slug} itemscope="itemscope" itemtype="https://schema.org/Article">
        <Link to={edge.node.fields.slug} className={styles['feed__item-thumbnail']}
              onClick={() => gtagTrack('ImageLink', 'click', edge.node.fields.slug, { title: edge.node.frontmatter.title })} title={edge.node.frontmatter.title}>
          <link rel="preload" href={edge.node.frontmatter.thumbnail} as="image" />
          <img src={edge.node.frontmatter.thumbnail} loading="lazy" alt={edge.node.frontmatter.title} itemprop="image" />
        </Link>
        <h2 className={styles['feed__item-title']} itemprop="name">
          <Link className={styles['feed__item-title-link']} to={edge.node.fields.slug}
            onClick={() => gtagTrack('PostLink', 'click', edge.node.fields.slug, { title: edge.node.frontmatter.title })} title={edge.node.frontmatter.title} itemprop="url">{edge.node.frontmatter.title}</Link>
        </h2>
        <div className={styles['feed__item-meta']}>
          <time className={styles['feed__item-meta-time']} dateTime={ new Date(edge.node.frontmatter.date).toLocaleDateString('vi-VN', { year: 'numeric', month: 'long', day: 'numeric' })}>
          {/* new Date(edge.node.frontmatter.date).toLocaleDateString('vi-VN', { year: 'numeric', month: 'long', day: 'numeric' }) */}
          { new Date(edge.node.frontmatter.date).toLocaleDateString('vi-VN', { year: 'numeric', month: 'long' }) }
          </time>
          <span className={styles['feed__item-meta-divider']} />
          <span className={styles['feed__item-meta-category']}>
            <Link to={edge.node.fields.categorySlug} className={styles['feed__item-meta-category-link']}
              onClick={() => gtagTrack('CategoryLink', 'click', edge.node.fields.categorySlug)} title={edge.node.frontmatter.category}>{edge.node.frontmatter.category}</Link>
          </span>
        </div>
        <p className={styles['feed__item-description']} itemprop="description">{edge.node.frontmatter.description}</p>
        {/* <Link className={styles['feed__item-readmore']} to={edge.node.fields.slug}>Read</Link> */}
        {/* <Link className={styles['feed__item-readmore']} to={edge.node.fields.slug}>{' '}</Link> */}
      </article>
    ))}
  </div>
);

export default Feed;