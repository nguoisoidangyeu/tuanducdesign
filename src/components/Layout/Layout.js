// @flow strict
import React from 'react';
import Helmet from 'react-helmet';
import type { Node as ReactNode } from 'react';
import styles from './Layout.module.scss';

type Props = {
  children: ReactNode,
  title: string,
  description?: string
};

const Layout = ({ children, title, description }: Props) => (
  <div className={styles.layout}>
    <Helmet>
      <html lang="vi" />
      <title>{title}</title>
      <meta name="title" content={title} />
      <meta name="description" content={description} />
      <meta itemprop="description" content={description} />
      <meta property="og:locale" content="vi" />
      <meta property="og:title" content={title} />
      <meta property="fb:admins" content="100005485267478" />
      <meta property="fb:app_id" content="294587991860787" />
      <meta property="og:description" content={description} />
      <meta property="og:site_name" content="Tuan Duc Design" />
      <meta name="twitter:title" content={title} />
      <meta name="twitter:description" content={description} />
      <meta name="twitter:site" content="@tuanducdesign" />
      <meta name="twitter:creator" content="@tuanducdesign" />
      <meta name="p:domain_verify" content="28151df45c2d2e4fd63414a7983dd4d1" />
      <meta name="robots" content="index,follow,all" />
    </Helmet>
    {children}
  </div>
);

export default Layout;