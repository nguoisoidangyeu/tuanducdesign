// @flow strict
import React from 'react';
import Helmet from 'react-helmet';
import Sidebar from '../components/Sidebar';
import Layout from '../components/Layout';
import Page from '../components/Page';
import { useSiteMetadata } from '../hooks';

const NotFoundTemplate = () => {
  const { title, subtitle } = useSiteMetadata();

  return (
    <Layout title={`${title}`} description={subtitle}>
    <Helmet>
    <meta name="image" content="https://tuanducdesign.com/CA0E67E9-AAD0-4D36-82D8-674C7504DFD1.jpg" />
    <meta itemprop="image" content="https://tuanducdesign.com/CA0E67E9-AAD0-4D36-82D8-674C7504DFD1.jpg" />
    <meta property="og:type" content="website" />
    <meta property="og:image" content="https://tuanducdesign.com/CA0E67E9-AAD0-4D36-82D8-674C7504DFD1.jpg" />
    <meta property="og:image:alt" content="Tuan Duc Design" />
    <meta name="twitter:card" content="summary_large_image" />
    <meta name="twitter:image" content="https://tuanducdesign.com/CA0E67E9-AAD0-4D36-82D8-674C7504DFD1.jpg" />
    <meta name="twitter:image:alt" content="Tuan Duc Design" />
    </Helmet>
      <Sidebar />
      <Page title="Bạn hiện không xem được nội dung này">
        <p>Nội dung bạn cần tìm có thể không tồn tại hoặc đã bị xoá.</p>
      </Page>
    </Layout>
  );
};

export default NotFoundTemplate;
