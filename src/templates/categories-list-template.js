// @flow strict
import React from 'react';
import Helmet from 'react-helmet';
import { Link } from 'gatsby';
import kebabCase from 'lodash/kebabCase';
import loadable from '@loadable/component';
const Sidebar = loadable(() =>
  import(/* webpackPrefetch: true */ '../components/Sidebar'),
);
import Layout from '../components/Layout';
const Page = loadable(() =>
  import(/* webpackPrefetch: true */ '../components/Page'),
);
const CategoryTag = loadable(() =>
  import(/* webpackPrefetch: true */ '../components/Layout/CategoryTag'),
);
import { useSiteMetadata, useCategoriesList } from '../hooks';

const topCategories = ['HTML', 'Javascript', 'Library'];


const CategoriesListHighlight = ({ categories }) => (
    <div style={{ marginBottom: 30 }}>
      {categories.map((category) => <CategoryTag category={category} key={category} />)}
    </div>
);

const CategoriesListTemplate = () => {
  const { title, subtitle } = useSiteMetadata();
  const categories = useCategoriesList();

  return (
    <Layout title={`Thể loại - ${title}`} description={subtitle}>
    <Helmet>
    <meta name="image" content="https://tuanducdesign.com/CA0E67E9-AAD0-4D36-82D8-674C7504DFD1.jpg" />
    <meta itemprop="image" content="https://tuanducdesign.com/CA0E67E9-AAD0-4D36-82D8-674C7504DFD1.jpg" />
    <meta property="og:type" content="website" />
    <meta property="og:image" content="https://tuanducdesign.com/CA0E67E9-AAD0-4D36-82D8-674C7504DFD1.jpg" />
    <meta property="og:image:alt" content="Tuan Duc Design" />
    <meta name="twitter:card" content="summary_large_image" />
    <meta name="twitter:image" content="https://tuanducdesign.com/CA0E67E9-AAD0-4D36-82D8-674C7504DFD1.jpg" />
    <meta name="twitter:image:alt" content="Tuan Duc Design" />
    </Helmet>
      <Sidebar />
      <Page title="Thể loại">
        <CategoriesListHighlight categories={topCategories} />

        <ul>
          {categories.map((category) => (
            <li key={category.fieldValue}>
              <Link title={category.fieldValue} to={`/category/${kebabCase(category.fieldValue)}/`}>
                {category.fieldValue} ({category.totalCount})
              </Link>
            </li>
          ))}
        </ul>
      </Page>
    </Layout>
  );
};

export default CategoriesListTemplate;
