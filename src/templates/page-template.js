// @flow strict
import React from 'react';
import Helmet from 'react-helmet';
import { graphql } from 'gatsby';
import Layout from '../components/Layout';
import Sidebar from '../components/Sidebar';
import Page from '../components/Page';
import { useSiteMetadata } from '../hooks';
import type { MarkdownRemark } from '../types';

type Props = {
  data: {
    markdownRemark: MarkdownRemark
  }
};

const PageTemplate = ({ data }: Props) => {
  const { title: siteTitle, subtitle: siteSubtitle } = useSiteMetadata();
  const { html: pageBody } = data.markdownRemark;
  const { title: pageTitle, description: pageDescription } = data.markdownRemark.frontmatter;
  const metaDescription = pageDescription !== null ? pageDescription : siteSubtitle;

  return (
    <Layout title={`${pageTitle} - ${siteTitle}`} description={metaDescription}>
    <Helmet>
    <meta name="image" content="https://tuanducdesign.com/CA0E67E9-AAD0-4D36-82D8-674C7504DFD1.jpg" />
    <meta itemprop="image" content="https://tuanducdesign.com/CA0E67E9-AAD0-4D36-82D8-674C7504DFD1.jpg" />
    <meta property="og:type" content="article" />
    <meta property="og:image" content="https://tuanducdesign.com/CA0E67E9-AAD0-4D36-82D8-674C7504DFD1.jpg" />
    <meta property="og:image:alt" content="Tuan Duc Design" />
    <meta name="twitter:card" content="summary_large_image" />
    <meta name="twitter:image" content="https://tuanducdesign.com/CA0E67E9-AAD0-4D36-82D8-674C7504DFD1.jpg" />
    <meta name="twitter:image:alt" content="Tuan Duc Design" />
    </Helmet>
      <Sidebar />
      <Page title={pageTitle}>
        <div dangerouslySetInnerHTML={{ __html: pageBody }} />
      </Page>
    </Layout>
  );
};

export const query = graphql`
  query PageBySlug($slug: String!) {
    markdownRemark(fields: { slug: { eq: $slug } }) {
      id
      html
      frontmatter {
        title
        date
        description
      }
    }
  }
`;

export default PageTemplate;
