// @flow strict
import React from 'react';
import Helmet from 'react-helmet';
import { graphql } from 'gatsby';
import loadable from '@loadable/component';
import Layout from '../components/Layout';
const Sidebar = loadable(() =>
  import(/* webpackPrefetch: true */ '../components/Sidebar'),
);
const Feed = loadable(() =>
  import(/* webpackPrefetch: true */ '../components/Feed'),
);
const Page = loadable(() =>
  import(/* webpackPrefetch: true */ '../components/Page'),
);
import Pagination from '../components/Pagination';
import { useSiteMetadata } from '../hooks';
import type { PageContext, AllMarkdownRemark } from '../types';

type Props = {
  data: AllMarkdownRemark,
  pageContext: PageContext
};

const IndexTemplate = ({ data, pageContext }: Props) => {
  const { title: siteTitle, subtitle: siteSubtitle } = useSiteMetadata();

  const {
    currentPage,
    hasNextPage,
    hasPrevPage,
    prevPagePath,
    nextPagePath
  } = pageContext;


  const { edges } = data.allMarkdownRemark;
  const pageTitle = currentPage > 0 ? `Bài viết - Trang ${currentPage} - ${siteTitle}` : siteTitle;

  return (
    <Layout title={pageTitle} description={siteSubtitle}>
    <Helmet>
    <meta name="image" content="https://tuanducdesign.com/CA0E67E9-AAD0-4D36-82D8-674C7504DFD1.jpg" />
    <meta itemprop="image" content="https://tuanducdesign.com/CA0E67E9-AAD0-4D36-82D8-674C7504DFD1.jpg" />
    <meta property="og:type" content="website" />
    <meta property="og:image" content="https://tuanducdesign.com/CA0E67E9-AAD0-4D36-82D8-674C7504DFD1.jpg" />
    <meta property="og:image:alt" content="Tuan Duc Design" />
    <meta name="twitter:card" content="summary_large_image" />
    <meta name="twitter:image" content="https://tuanducdesign.com/CA0E67E9-AAD0-4D36-82D8-674C7504DFD1.jpg" />
    <meta name="twitter:image:alt" content="Tuan Duc Design" />
    </Helmet>
      <Sidebar isIndex />
      <Page>
        <Feed edges={edges} />
        <Pagination
          prevPagePath={prevPagePath}
          nextPagePath={nextPagePath}
          hasPrevPage={hasPrevPage}
          hasNextPage={hasNextPage}
        />
      </Page>
    </Layout>
  );
};

export const query = graphql`
  query IndexTemplate($postsLimit: Int!, $postsOffset: Int!) {
    allMarkdownRemark(
        limit: $postsLimit,
        skip: $postsOffset,
        filter: { frontmatter: { template: { eq: "post" }, draft: { ne: true } } },
        sort: { order: DESC, fields: [frontmatter___date] }
      ){
      edges {
        node {
          fields {
            slug
            categorySlug
          }
          frontmatter {
            title
            date
            category
            description
            thumbnail
          }
        }
      }
    }
  }
`;

export default IndexTemplate;
