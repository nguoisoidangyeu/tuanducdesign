// @flow strict
import React from 'react';
import Helmet from 'react-helmet';
import { Link } from 'gatsby';
import kebabCase from 'lodash/kebabCase';
import Layout from '../components/Layout';
import Sidebar from '../components/Sidebar';
import Page from '../components/Page';
import { useSiteMetadata, useTagsList } from '../hooks';

const TagsListTemplate = () => {
  const { title, subtitle } = useSiteMetadata();
  const tags = useTagsList();

  return (
    <Layout title={`Thẻ - ${title}`} description={subtitle}>
    <Helmet>
    <meta name="image" content="https://tuanducdesign.com/CA0E67E9-AAD0-4D36-82D8-674C7504DFD1.jpg" />
    <meta itemprop="image" content="https://tuanducdesign.com/CA0E67E9-AAD0-4D36-82D8-674C7504DFD1.jpg" />
    <meta property="og:type" content="article" />
    <meta property="og:image" content="https://tuanducdesign.com/CA0E67E9-AAD0-4D36-82D8-674C7504DFD1.jpg" />
    <meta property="og:image:alt" content="Tuan Duc Design" />
    <meta name="twitter:card" content="summary_large_image" />
    <meta name="twitter:image" content="https://tuanducdesign.com/CA0E67E9-AAD0-4D36-82D8-674C7504DFD1.jpg" />
    <meta name="twitter:image:alt" content="Tuan Duc Design" />
    </Helmet>
      <Sidebar />
      <Page title="Thẻ">
        <ul>
          {tags.map((tag) => (
            <li key={tag.fieldValue}>
              <Link title={tag.fieldValue} to={`/tag/${kebabCase(tag.fieldValue)}/`}>
                {tag.fieldValue} ({tag.totalCount})
              </Link>
            </li>
          ))}
        </ul>
      </Page>
    </Layout>
  );
};

export default TagsListTemplate;
