/* eslint-disable no-alert */
/* eslint-disable import/prefer-default-export */

'use strict';

require('./src/assets/scss/init.scss');
require('./static/css/prismjs/theme.scss');
export const onServiceWorkerUpdateReady = () => {
  const answer = window.confirm(
    'Blog này đã được cập nhật. '
      + 'Tải lại trang để hiển thị phiên bản mới nhất?'
  );
  if (answer === true) {
    window.location.reload();
  }
};
